from __future__ import unicode_literals
from frappe import _

def get_data():
	return [
		{
			"label": _("Replacement Management"),
			"items": [
                {
					"type": "doctype",
					"name": "P2P Request",
					"onboard": 1,
				},
				{
				"type": "doctype",
				"name": "Regular Replacement Request",
				"onboard": 1,
				},
				{
					"type": "doctype",
					"name": "Dealer Registration",
					"onboard": 1,
				}
			]
		},
		{
			"label": _("Replacement Masters"),
			"items": [
                
				{
					"type": "doctype",
					"name": "RMA Master",
					"onboard": 1,
				},
				{
					"type": "doctype",
					"name": "P2P Master",
					"onboard": 1,
				},
			]
		},
		
		
	]
