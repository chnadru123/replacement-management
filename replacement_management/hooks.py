# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from . import __version__ as app_version

app_name = "replacement_management"
app_title = "Replacement Management"
app_publisher = "Raaj Tailor"
app_description = "Replacement Management"
app_icon = "octicon octicon-file-directory"
app_color = "grey"
app_email = "tailorraj111@gmail.com"
app_license = "MIT"


# fixtures = [{"dt":"Role", "filters": [["name", "in", ["RMA Draft","Dealer","Distributor","RMA Engineer","RMA Admin"]]]},{"dt":"Custom DocPerm", "filters": [["role", "in", ["RMA Draft","Dealer","Distributor","RMA Engineer","RMA Admin"]]]},{"dt":"Workflow", "filters": [["document_type", "=", "RMA"]]},"Workflow State","Workflow Action Master"]

fixtures = [{"dt":"Role", "filters": [["name", "in", ["RMA Draft","Dealer","Distributor","RMA Engineer","RMA Admin"]]]},{"dt":"Custom DocPerm", "filters": [["role", "in", ["RMA Draft","Dealer","Distributor","RMA Engineer","RMA Admin"]]]},{"dt":"Workflow", "filters": [["document_type", "in", ["P2P Request","Regular Replacement Request","Dealer Registration"]]]},"Workflow State","Workflow Action Master",{"dt":"Custom Field", "filters": [["name", "in", ["rr_reference","Sales Order Item-reference_dn","Sales Order Item-reference_dt","Sales Order-rr_reference","Sales Invoice-rr_reference"]]]}]

# Includes in <head>
# ------------------

# include js, css files in header of desk.html
# app_include_css = "/assets/replacement_management/css/replacement_management.css"
# app_include_js = "/assets/replacement_management/js/replacement_management.js"

# include js, css files in header of web template
# web_include_css = "/assets/replacement_management/css/replacement_management.css"
# web_include_js = "/assets/replacement_management/js/replacement_management.js"

# include js in page
# page_js = {"page" : "public/js/file.js"}

# include js in doctype views
# doctype_js = {"doctype" : "public/js/doctype.js"}
# doctype_list_js = {"doctype" : "public/js/doctype_list.js"}
# doctype_tree_js = {"doctype" : "public/js/doctype_tree.js"}
# doctype_calendar_js = {"doctype" : "public/js/doctype_calendar.js"}

# Home Pages
# ----------

# application home page (will override Website Settings)
# home_page = "login"

# website user home page (by Role)
# role_home_page = {
#	"Role": "home_page"
# }

# Website user home page (by function)
# get_website_user_home_page = "replacement_management.utils.get_home_page"

# Generators
# ----------

# automatically create page for each record of this doctype
# website_generators = ["Web Page"]

# Installation
# ------------

# before_install = "replacement_management.install.before_install"
# after_install = "replacement_management.install.after_install"

# Desk Notifications
# ------------------
# See frappe.core.notifications.get_notification_config

# notification_config = "replacement_management.notifications.get_notification_config"

# Permissions
# -----------
# Permissions evaluated in scripted ways
permission_query_conditions = {
	"RMA": "replacement_management.replacement_management.doctype.rma.rma.get_permission_query_conditions",
	"P2P Request":"replacement_management.replacement_management.doctype.p2p_request.p2p_request.get_permission_query_conditions",
	"Regular Replacement Request":"replacement_management.replacement_management.doctype.regular_replacement_request.regular_replacement_request.get_permission_query_conditions"
}
# permission_query_conditions = {
# 	"Event": "frappe.desk.doctype.event.event.get_permission_query_conditions",
# }
#
# has_permission = {
# 	"Event": "frappe.desk.doctype.event.event.has_permission",
# }

# Document Events
# ---------------
# Hook on document methods and events

# doc_events = {
# 	"*": {
# 		"on_update": "method",
# 		"on_cancel": "method",
# 		"on_trash": "method"
#	}
# }
doc_events = {
	"Sales Invoice" :{
		"on_submit":"replacement_management.replacement_management.sales_invoice.on_submit"
		
	}

}


# Scheduled Tasks
# ---------------

# scheduler_events = {
# 	"all": [
# 		"replacement_management.tasks.all"
# 	],
# 	"daily": [
# 		"replacement_management.tasks.daily"
# 	],
# 	"hourly": [
# 		"replacement_management.tasks.hourly"
# 	],
# 	"weekly": [
# 		"replacement_management.tasks.weekly"
# 	]
# 	"monthly": [
# 		"replacement_management.tasks.monthly"
# 	]
# }

# Testing
# -------

# before_tests = "replacement_management.install.before_tests"

# Overriding Methods
# ------------------------------
#
# override_whitelisted_methods = {
# 	"frappe.desk.doctype.event.event.get_events": "replacement_management.event.get_events"
# }
#
# each overriding function accepts a `data` argument;
# generated from the base implementation of the doctype dashboard,
# along with any modifications made in other Frappe apps
# override_doctype_dashboards = {
# 	"Task": "replacement_management.task.get_dashboard_data"
# }

