import frappe

def on_submit(self,method):
    if self.rr_reference:
        frappe.db.set_value("Regular Replacement Request",self.rr_reference,"sales_invoice_ref",self.name)