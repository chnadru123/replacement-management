// Copyright (c) 2016, Raaj Tailor and contributors
// For license information, please see license.txt
/* eslint-disable */

frappe.query_reports["RMA Status"] = {
	"filters": [
		{
			"fieldname":"from_date",
			"label": __("From Date"),
			"fieldtype": "Date",
			"default": frappe.datetime.get_today(),
			
			"width": "60px"
		},
		{
			"fieldname":"to_date",
			"label": __("To Date"),
			"fieldtype": "Date",
			"default": frappe.datetime.get_today(),
			
			"width": "60px"
		},

	]
};
