// Copyright (c) 2020, Raaj Tailor and contributors
// For license information, please see license.txt

frappe.ui.form.on('Dealer Registration', {
	refresh(frm) {
		cur_frm.add_custom_button(__('Get Customer Data'), function() {
            frappe.call({
                "method":"frappe.client.get",
                "args":{
                    "doctype":"Customer",
                    "filters":{
                        "tax_id":frm.doc.gstn
                    }
                },
                "callback":function(res){
                    if(res.message){
                        
                        frm.set_value("registered_name",res.message.name)
                        frm.refresh_field("registered_name")
                        
                        if(res.message.customer_primary_address){
                            frappe.call({
                            "method":"frappe.client.get",
                            "args":{
                                "doctype":"Address",
                                "filters":{
                                    "name":res.message.customer_primary_address
                                }
                            },
                            "callback":function(res){
                            
                            frm.set_value("gst_number",res.message.gstin)
                            frm.set_value("registered_state",res.message.gst_state)
                            frm.set_value("registered_pincode",res.message.pincode)
                            frm.set_value("registered_number",res.message.phone)
                            frm.refresh_field("gst_number","registered_state","registered_pincode","registered_number")
                            frm.save()
                            
                            }
                                
                            })
                            
                        }else{
							frappe.msgprint("No Address Found For Customer!")
							frm.save()
                            
                        }
                        console.log(res)
                    }
                }
            })
        });
	}
})

cur_frm.fields_dict['distributor_details'].grid.get_field('distributor').get_query = function(doc) {
	return {
		 filters: { 'customer_group': "Distributor" }
	}
}

// frappe.ui.form.on('Distributor Details', {
// 	setup: function(frm) {
// 		frm.set_query("distributor", function() {
// 			return {
// 				filters: [
// 					["Customer","customer_group", "=", "Distributor"]
// 				]
// 			}
// 		});
// 	}
// })
