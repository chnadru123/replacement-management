// Copyright (c) 2021, Raaj Tailor and contributors
// For license information, please see license.txt

frappe.ui.form.on('RMA Master Admin', {
	refresh(frm) {
		// your code here
	},
	on_submit(frm){
	    frm.doc.regular_replacement.forEach(element => {
           
			frappe.db.set_value("Regular Replacement Request",element.rr_voucher,"workflow_state","Dispatched")
        });
	},
	
	onload:async function(frm){
	    let company = await frappe.db.get_value("Global Defaults",null,"default_company")
	    console.log(company.message)
	    if(company.message){
	        frm.set_value("company",company.message.default_company)
	        frm.refresh_field("company")
            frappe.call({
                method: "frappe.client.get",
                args: {
                    doctype: "Address",
                    filters: {
                	is_your_company_address: ["=", 1 ],
                
                   },
                },
                callback(r) {
                    if(r.message) {
                       //var task = r.message;
                    //    console.log(task);
                     cur_frm.set_value('company_address',r.message.address_line1+r.message.address_line2+','+r.message.city+','+r.message.country) 
                   
                    }
                }
            });
	    }
	}
})

cur_frm.fields_dict['rma_list'].grid.get_field('rma_no').get_query = function(doc) {
	return {
		 filters: {
		     "workflow_state": ["in", ['Ready to Dispatch']],
		     "customer_name": doc.customer
		 }
		 
	
		 
		 
	}
}
cur_frm.fields_dict['regular_replacement'].grid.get_field('rr_voucher').get_query = function(doc) {
	return {
		
		 filters: {
		     "workflow_state": ["in", ['Ready to Dispatch']]

		 }
		 
	
		 
		 
	}
}