// Copyright (c) 2021, Raaj Tailor and contributors
// For license information, please see license.txt

frappe.ui.form.on('Regular Replacement Request', {
	refresh: function(frm) {
		console.log($("[data-label='Send to Company']").parent().parent().parent().parent())
		if(frm.doc.workflow_state == "Draft"){
			// $("[data-label='Send to Company']").parent().hide()
			$("[data-label='Send to Company']").parent().parent().parent().parent().hide()
		}else{
			$("[data-label='Send to Company']").parent().parent().parent().parent().show()
		}
		if(frm.doc.workflow_state == "Ready to Dispatch"){
			console.log("here")
			// $("[data-label='Send to Company']").parent().hide()
			$("[data-label='Dispatch']").parent().parent().parent().parent().hide()
		}else{
			$("[data-label='Dispatch']").parent().parent().parent().parent().show()
		}
		
		if(frm.doc.workflow_state == "Ready to Dispatch" && frm.doc.approval_required == "Yes" && frm.doc.repair_material_required && frappe.user.has_role("RMA Admin")){
			cur_frm.add_custom_button(__('Create Sales Invoice'), function() {
				frappe.model.open_mapped_doc({
					method: "replacement_management.replacement_management.doctype.regular_replacement_request.regular_replacement_request.get_map_doc",
					frm: cur_frm
					})
		
			})
		}
		if(frm.doc.workflow_state == "Dispatched"){
			cur_frm.add_custom_button(__('Stock Ledger'), function() {
				frappe.route_options = {
				"voucher_no": cur_frm.doc.name,
				"from_date": cur_frm.doc.date,
				"to_date": cur_frm.doc.date,
				"company": "Trueview",
				group_by_voucher: 0,
				group_by_account: 0,
				};
				frappe.set_route("query-report", "Stock Ledger");
			}, "View");
		}
		

	},
	workflow_state:function(frm){
		if(frm.doc.workflow_state == "Draft"){
			$("[data-label='Send to Company']").parent().hide()
		}
	},
	before_save:async function(frm){
		if(!frm.doc.terms_check)
		{

			var terms_details = await frappe.db.get_value("Terms and Conditions","RR Terms","terms")
			if(terms_details.message.terms){
				return new Promise(function(resolve, reject) {
					frappe.confirm(
						terms_details.message.terms,
						function() {
							var negative = 'frappe.validated = false';
							resolve(negative);
							frm.set_value("terms_check",1)
							frm.set_value("agree_to_terms_and_conditions",1)
							frm.refresh_field("terms_check","agree_to_terms_and_conditions")
						},
						function() {
							reject();
						}
					)
				})

			}
		}
		// console.log(terms_details.message.terms)
		

	},
	setup: function(frm) {
	    console.log(frappe.session.user)
		frm.set_query("distributor_name", function() {
			return {
				filters: {
				    "user_name":frappe.session.user
				},
				query:"replacement_management.replacement_management.doctype.regular_replacement_request.regular_replacement_request.filter_distributor"
			}
		});
		frm.set_query("rma_engineer", function() {
			return {
				query:"replacement_management.replacement_management.doctype.regular_replacement_request.regular_replacement_request.get_rma_engineer"
			}
		});
	},
	onload: function(frm) {
		console.log(frappe.session.user)
		if(frm.doc.name.search("New Regular Replacement") >= 0){
			frappe.call({
				"method":"get_customer_details",
				"doc":frm.doc,
				"args":{"uname":frappe.session.user},
				"callback":function(res){
					console.log(res)
					if( res.message[0]){
						frm.set_value("customer_name",res.message[0])
						frm.refresh_field("customer_name")
					}
					if( res.message[1]){
						frm.set_value("customer_mobile_no",res.message[1])
						frm.refresh_field("customer_mobile_no")
					}
	
				}
	
			})

		}
		
		
	},
	validate:function(frm){
		var total= 0
		frm.doc.repair_material_required.forEach(element => {
			total = element.rate_including_tax + total
		});
		frm.set_value("total_amount",total)
		frm.refresh_field("total_amount")
	}
});

cur_frm.fields_dict['repair_material_required'].grid.get_field('item_code').get_query = function(doc) {
	return {
		 filters: {
		     "item_group": ["=", 'Raw Material']
		 }
		 
	
		 
		 
	}
}


frappe.ui.form.on('RMA Items', {
	rate:function(frm,cdt,cdn){
		var row = locals[cdt][cdn]
		if(row.rate){
			console.log(row.rate)
			var wo_gst = row.rate*1.18
			row.rate_including_tax = wo_gst
			frm.refresh_field("repair_material_required")
		}
	}
})