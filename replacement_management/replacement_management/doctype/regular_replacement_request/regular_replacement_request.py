# -*- coding: utf-8 -*-
# Copyright (c) 2021, Raaj Tailor and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
from frappe.contacts.doctype.contact.contact import get_contact_name
from erpnext.controllers.stock_controller import StockController



@frappe.whitelist()
def filter_distributor(doctype, txt, searchfield, start, page_len, filters):
	dr = frappe.db.get_value("Dealer Registration",{"user_id":filters.get("user_name")},"name")
	response = frappe.db.get_list("Distributor Details",{"parent":dr},["distributor"],as_list=1)
	# frappe.msgprint(str(response))
	
	return response

class RegularReplacementRequest(StockController,Document):
	def get_customer_details(self,uname):
		party = None
		contact_name = get_contact_name(uname)
		if contact_name:
			# frappe.msgprint(str(contact_name))
			contact = frappe.get_doc('Contact', contact_name)

			if contact.links:
				party_doctype = contact.links[0].link_doctype
				party = contact.links[0].link_name
		if party:
			# frappe.msgprint(str(party))
			cust_doc = frappe.get_doc(party_doctype, party)
			return (cust_doc.customer_name,cust_doc.mobile_no)
	def validate(self):
		if self.workflow_state == "Ready to Dispatch" and self.approval_required == "No" and not self.stock_deducted:
			for item in self.repair_material_required:
				self.deduct_stock_from_warehouse({
					"deduct_item":item.item_code,
					"deduct_warehouse":"Replacement - T"
				})
			self.stock_deducted = 1
		


	def deduct_stock_from_warehouse(self,args):
		
		sl_entries = []
		sl_entries.append(frappe._dict({
		"item_code": args['deduct_item'],
		"warehouse": args['deduct_warehouse'],
		"posting_date": self.date,				
		"voucher_type": self.doctype,
		"voucher_no": self.name,
		"actual_qty": -1,				
		"company": "Trueview",				
		"is_cancelled": self.docstatus==2 and "Yes" or "No"
		}))
		# frappe.throw(str(sl_entries))
		self.make_sl_entries(sl_entries, self.docstatus==2 and 'Yes' or 'No')

@frappe.whitelist()
def get_map_doc(source_name, target_doc=None):
 
    from frappe.model.mapper import get_mapped_doc
 
    def set_missing_values(source, target):
        pass
    
    def update_item(obj, target, source_parent):
        target.qty = 1
        target.description = obj.item_name
        target.uom = frappe.db.get_value("Item",obj.item_code,"stock_uom")
        target.income_account = "Sales - T"
        target.reference_dt = "Regular Replacement Request"
        target.reference_dn = source_parent.name
            
    doc = get_mapped_doc("Regular Replacement Request", source_name,   {
        "Regular Replacement Request": {
            "doctype": "Sales Order",
            "field_map": {
                "name":"rr_reference",
				"customeradmin":"customer"
            }
        },
		"RMA Items":{
			"doctype":"Sales Order Item",
			"field_map":{
				
			},
			"postprocess": update_item
		}
 
    }, target_doc, set_missing_values)
    doc.ref_voucher = "Regular Replacement Request"
 
    return doc


@frappe.whitelist()
def get_permission_query_conditions(user):
	# customer_name = frappe.db.get_value("Dealer Registration",{"user_id":frappe.session.user},"registered_name")
	
	if "RMA Admin" in frappe.get_roles(user):
		return None
	elif "RMA Engineer" in frappe.get_roles(user):
		return None
		# return """\
		#  (`tabRegular Replacement Request`.name in (select tabToDo.reference_name from tabToDo where
		#  	(tabToDo.owner = {user} or tabToDo.assigned_by = {user}) 
		#  	and tabToDo.reference_type = 'Regular Replacement Request' and tabToDo.reference_name=`tabRegular Replacement Request`.name and tabToDo.status = 'Open'))\
		#  """.format(user=frappe.db.escape(user))
	else:
		return "(`tabRegular Replacement Request`.owner = {user}) or (`tabRegular Replacement Request`.customer_emailadmin = {user}) or (`tabRegular Replacement Request`.distributor_email = {user})".format(user = frappe.db.escape(user))
		# return """(tabRMA.customer_name = {customer})""".format(customer=customer_name)

@frappe.whitelist()
def get_rma_engineer(doctype, txt, searchfield, start, page_len, filters):
	return frappe.desk.reportview.execute("User", filters = [["Has Role","role","=","RMA Engineer"]],fields = ["name"],limit_start=0, limit_page_length=100, order_by = "name", as_list=True)