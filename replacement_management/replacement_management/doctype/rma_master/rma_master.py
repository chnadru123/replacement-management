# -*- coding: utf-8 -*-
# Copyright (c) 2020, Raaj Tailor and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
from frappe.contacts.doctype.contact.contact import get_contact_name

class RMAMaster(Document):
	def get_customer_details(self,uname):
		party = None
		contact_name = get_contact_name(uname)
		if contact_name:
			# frappe.msgprint(str(contact_name))
			contact = frappe.get_doc('Contact', contact_name)

			if contact.links:
				party_doctype = contact.links[0].link_doctype
				party = contact.links[0].link_name
		if party:
			# frappe.msgprint(str(party))
			cust_doc = frappe.get_doc(party_doctype, party)
			return (cust_doc.customer_name,cust_doc.mobile_no)
	def validate(self):
		# pass
		rma_list = []
		for item in self.rr_list:
			if item.rr_voucher in rma_list:
				frappe.throw("Regular Replace No {} is Repeating".format(item.rr_voucher))
			else:
				rma_list.append(item.rr_voucher)
	def get_company_add(self):
		add_doc =  frappe.get_doc("Address",{"is_your_company_address":1})
		add_str = add_doc.address_line1 +","+ add_doc.address_line2 if add_doc.address_line2 else "" +","+ add_doc.city +","+ add_doc.state
		return add_str
