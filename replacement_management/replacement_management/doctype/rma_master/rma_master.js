// Copyright (c) 2020, Raaj Tailor and contributors
// For license information, please see license.txt

frappe.ui.form.on('RMA Master', {
	
	onload: async function(frm) {
		if(frm.doc.name.search("New RMA Master") >= 0){
			frappe.call({
				"method":"get_customer_details",
				"doc":frm.doc,
				"args":{"uname":frappe.session.user},
				"callback":function(res){
					console.log(res)
					if( res.message[0]){
						frm.set_value("customer",res.message[0])
						frappe.db.get_value("Customer",res.message[0],"customer_group").then(function(res){
							console.log(res)
							frm.set_value("customer_group",res.message.customer_group)
						})
						frm.refresh_field("customer")
					}
					
	
				}
	
			})

		}

		frappe.db.get_value("Global Defaults",null,"default_company").then(function(res){
			console.log(res.message.default_company)
			frm.set_value("company",res.message.default_company)
			frm.refresh_field("company")

		})
		frappe.call({
			"method":"get_company_add",
			"doc":frm.doc,
			"callback":function(res){
				console.log(res)
				if(res.message){
					frm.set_value("company_address",res.message)
					frm.refresh_field("company_address")
				}
				
			}

		})

	},
	on_submit(frm){
	    frm.doc.rma_list.forEach(element => {
            // console.log(element.rma_no)
            // frappe.db.set_value("RMA",element.rma_no,"workflow_state","Sent to Unit")
			frappe.db.set_value("P2P Request",element.p2p,"workflow_state","Sent to Unit")
        });
		frm.doc.rr_list.forEach(element => {
            // console.log(element.rma_no)
            // frappe.db.set_value("RMA",element.rma_no,"workflow_state","Sent to Unit")
			frappe.db.set_value("Regular Replacement Request",element.rr_voucher,"workflow_state","Sent to Company")
        });
	
	}
	
});


cur_frm.fields_dict['rma_list'].grid.get_field('p2p').get_query = function(doc) {
	if(doc.customer_group == "Distributor" || doc.customer_group == "Super Distributor"){
		return {
			 filters: {
				 "workflow_state": ["in", ['P2P Issued']]
			 }
		}
		
	}else{
		return {
			 filters: {
				 "workflow_state": ["in", ['P2P Approved']]
			 }
		}
	}
	
}

cur_frm.fields_dict['rr_list'].grid.get_field('rr_voucher').get_query = function(doc) {
	
		return {
			 filters: {
				 "workflow_state": ["in", ['Draft']]
			 }
		}
		
	
	
}